from os import getcwd, path, environ
from sys import argv
import re


def main():
    maxDepth = int(argv[1])

    # replace full home directory by ~
    cwd = re.sub('^{}'.format(environ['HOME']), '~', getcwd())

    head = path.split(cwd)[0]
    tail = path.split(cwd)[1]
    dirlist = [tail]

    while tail != '':
        tail = path.split(head)[1]
        head = path.split(head)[0]
        dirlist.append(tail)

    if maxDepth >= len(dirlist):
        print(cwd)
        return

    truncedPath = dirlist[maxDepth - 1]
    sep = path._get_sep(cwd)
    for i in range(maxDepth - 2, -1, -1):
        truncedPath += sep + dirlist[i]

    print(truncedPath)
    return


if __name__ == '__main__':
    main()
